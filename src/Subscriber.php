<?php declare(strict_types=1);

namespace Adduc\WebSub;

use GuzzleHttp\Client;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\UriInterface as Uri;
use Psr\Log\LoggerInterface as Logger;
use Psr\Log\NullLogger;
use function GuzzleHttp\Psr7\parse_query;
use GuzzleHttp\Psr7\Response;

class Subscriber
{
    const MODE_SUBSCRIBE = 'subscribe';
    const MODE_UNSUBSCRIBE = 'unsubscribe';

    const MODES = [
        self::MODE_SUBSCRIBE,
        self::MODE_UNSUBSCRIBE,
    ];

    /** @var Logger */
    protected $logger;

    /** @var Client */
    protected $client;

    public function __construct(?Logger $logger = null, ?Client $client = null)
    {
        if ($logger === null) {
            $logger = new NullLogger();
        }

        if ($client === null) {
            $client = new Client();
        }

        $this->logger = $logger;
        $this->client = $client;
    }

    /**
     * @todo add support for lease_seconds and secret fields
     */
    public function subscribe(Uri $hub, Uri $topic, Uri $callback): void
    {
        $this->action(self::MODE_SUBSCRIBE, $hub, $topic, $callback);
    }

    public function unsubscribe(Uri $hub, Uri $topic, Uri $callback): void
    {
        $this->action(self::MODE_UNSUBSCRIBE, $hub, $topic, $callback);
    }

    protected function action(string $mode, Uri $hub, Uri $topic, Uri $callback): void
    {
        if (!in_array($mode, self::MODES)) {
            throw new \InvalidArgumentException("Unknown mode ({$mode})");
        }

        $response = $this->client->post($hub, [
            'form_params' => [
                'hub.callback' => $callback->__toString(),
                'hub.mode' => $mode,
                'hub.topic' => $topic->__toString(),
            ],
            'http_errors' => false
        ]);

        $status_code = $response->getStatusCode();

        if ($status_code >= 400 && $status_code <= 599) {
            throw new Exception\SubscribeException($response->getBody()->__toString());
        }
    }

    public function verifySubscribe(Request $request, Uri $topic): Response
    {
        return $this->verify($request);
    }

    public function verifyUnsubscribe(Request $request, Uri $topic): Response
    {
        return $this->verify($request);
    }

    protected function verify(string $mode, Request $request, Uri $topic): Response
    {
        if (!in_array($mode, self::MODES)) {
            throw new \InvalidArgumentException("Unknown mode ({$mode})");
        }

        $query = $request->getUri()->getQuery();
        $query = parse_query($query);


        switch (true) {
            case empty($query['hub.topic']):
            case empty($query['hub.mode']):
            case empty($query['hub.challenge']):
            case $query['hub.topic'] != $topic->__toString():
                return new Response(404);
        }

        /** @todo identify how to return lease_seconds */
        return new Response(200, [], $query['hub.challenge']);
    }
}
