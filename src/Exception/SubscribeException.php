<?php declare(strict_types=1);

namespace Adduc\WebSub\Exception;

class SubscribeException extends \Exception
{
}
