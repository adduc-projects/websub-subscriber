<?php declare(strict_types=1);

namespace Adduc\WebSub;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use function GuzzleHttp\Psr7\uri_for;
use GuzzleHttp\Middleware;
use function GuzzleHttp\Psr7\parse_query;

class SubscriberTest extends TestCase
{
    /** @var array */
    protected $history = [];

    /**
     * @dataProvider provideSubscribeResponse
     */
    public function testSubscribeResponse(?string $exception, ResponseInterface ...$responses): void
    {
        $client = $this->getClient(...$responses);
        $subscriber = new Subscriber(null, $client);

        if ($exception) {
            $this->expectException($exception);
        }

        $uri = uri_for('https://example.com');
        $result = $subscriber->subscribe($uri, $uri, $uri);
        $this->assertNull($result);
    }

    public function provideSubscribeResponse(): array
    {
        $tests = [
            // Success: When status code of 202 is returned, expect null returned
            [null, new Response(202)],

            // Error: When server error (4XX/5XX), expect exception
            [Exception\SubscribeException::class, new Response(400)],
            [Exception\SubscribeException::class, new Response(500)],

            // Redirect: When server issues redirect to 200, expect null returned
            [null, new Response(307, ['Location' => '/']), new Response(200)],

            // Redirect: When server issues redirect to 500, expect null returned
            [Exception\SubscribeException::class, new Response(307, ['Location' => '/']), new Response(500)],
        ];

        return $tests;
    }

    /**
     * @dataProvider provideRequest
     */
    public function testRequest(string $mode, Uri $hub, Uri $topic, Uri $callback): void
    {
        $client = $this->getClient(new Response(202));
        $subscriber = new Subscriber(null, $client);

        if ($mode == Subscriber::MODE_SUBSCRIBE) {
            $subscriber->subscribe($hub, $topic, $callback);
        } elseif ($mode == Subscriber::MODE_UNSUBSCRIBE) {
            $subscriber->unsubscribe($hub, $topic, $callback);
        } else {
            throw new \Exception("Unknown mode");
        }

        $request = $this->history[0]['request'];

        $this->assertEquals($hub, $request->getUri());

        $body = $request->getBody()->__toString();
        $body = parse_query($body);

        $this->assertArrayHasKey('hub.callback', $body);
        $this->assertArrayHasKey('hub.mode', $body);
        $this->assertArrayHasKey('hub.topic', $body);

        $this->assertEquals($body['hub.callback'], $callback->__toString());
        $this->assertEquals($body['hub.mode'], $mode);
        $this->assertEquals($body['hub.topic'], $topic->__toString());
    }

    public function provideRequest(): array
    {
        $uris = [
            uri_for('https://example.com/1'),
            uri_for('https://example.com/2'),
            uri_for('https://example.com/3'),
        ];

        $tests = [
            [Subscriber::MODE_SUBSCRIBE, $uris[0], $uris[1], $uris[2]],
            [Subscriber::MODE_SUBSCRIBE, $uris[1], $uris[2], $uris[0]],
            [Subscriber::MODE_SUBSCRIBE, $uris[2], $uris[0], $uris[1]],

            [Subscriber::MODE_UNSUBSCRIBE, $uris[0], $uris[1], $uris[2]],
            [Subscriber::MODE_UNSUBSCRIBE, $uris[1], $uris[2], $uris[0]],
            [Subscriber::MODE_UNSUBSCRIBE, $uris[2], $uris[0], $uris[1]],
        ];

        return $tests;
    }

    /**
     * @dataProvider provideVerify
     */
    public function testVerify(string $mode, array $request_body, Uri $topic, int $expected_status_code): void
    {
        $subscriber = new Subscriber();

    }

    public function provideVerify(): array
    {
        $tests = [

        ];

        return $tests;
    }

    protected function getClient(ResponseInterface ...$responses): Client
    {
        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);

        $history = Middleware::history($this->history);
        $handler->push($history);

        $client = new Client(['handler' => $handler]);

        return $client;
    }
}
