WebSub Subscriber
===

### Specification

A conforming subscriber:

* MUST support each discovery mechanism in the specified order to
  discover the topic and hub URLs as described in Discovery.
* MUST send a subscription request as described in Subscriber Sends
  Subscription Request.
* MAY request a specific lease duration
* MAY include a secret in the subscription request, and if it does, then
  MUST use the secret to verify the signature in the content distribution request.
* MUST acknowledge a content distribution request with an HTTP 2xx status code.
* MAY request that a subscription is deactivated using the "unsubscribe" mechanism.


<!-- Link -->
[WebSub Specification]: https://www.w3.org/TR/websub/
